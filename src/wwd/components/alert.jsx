import React from 'react'
import PropTypes from 'prop-types'
import { classList } from '../utils/stringUtil'

const propTypes = {
  // Input
  error: PropTypes.shape({
    message: PropTypes.string,
    status: PropTypes.number
  }),
  render: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object
}
const defaultProps = {
  error: {
    message: 'An error occurred.',
    status: 0
  },
  render: err => <h5>{ err.message }</h5>,
  className: '',
  style: {}
}

/**
 * <h2>Input</h2>
 * error: {
 * --message: string,
 * --status (HTTP or Adler32; unknown = 0): number
 * }
 * render: (error: { message: string, status: number }) => node
 * className: string
 * style: object
 */
const Alert = props => {
  const { error, render, className, style } = props
  const alert = render(error)
  return React.cloneElement(alert, {
    className: classList(alert.props && alert.props.className, className),
    style: { ...(alert.props && alert.props.style), ...style }
  })
}

Alert.propTypes = propTypes
Alert.defaultProps = defaultProps

export default Alert
