import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { defaultComparator, keys } from '../../utils/arrayUtil'
import Orders from '../../enums/orders'
import DataTableHead from './dataTableHead'
import { classList, toReadable } from '../../utils/stringUtil'
import SizePicker from '../inputs/sizePicker'
import SearchInput from '../inputs/searchInput'
import PaginationButtons from '../paginationButtons'
import { Table } from 'reactstrap'
import DataTableBody from './dataTableBody'
import { innerText } from '../../utils/reactUtil'
import { resolveKey } from '../../utils/objectUtil'

const propTypes = {
  // Input
  data: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.object),
    PropTypes.string
  ]).isRequired,
  columns: PropTypes.arrayOf(PropTypes.object),
  classes: PropTypes.objectOf(PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func
  ])),
  filters: PropTypes.arrayOf(PropTypes.func),
  pagination: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.number
  ]),
  searchable: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.string
  ]),
  sortable: PropTypes.bool,
  defaultPage: PropTypes.number,
  error: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object,
  child: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.number
  ]),
  // Events
  onRowClick: PropTypes.func
}
const defaultProps = {
  // Input
  classes: {},
  filters: [],
  pagination: 10,
  searchable: true,
  sortable: true,
  defaultPage: 1,
  className: '',
  style: {},
  child: false,
  // Events
  onRowClick() {}
}

/**
 * <h2>Input</h2>
 *   data (required): object[] | string
 *   columns: {
 *   --key - the data key: string,
 *   --classes: {
 *   ----head(columnIndex: number) => string,
 *   ----body(columnData: object, rowIndex: number, columnIndex: number, rowData: object) => string
 *   --},
 *   --render(columnData: object, rowIndex: number, columnIndex: number, rowData: object) => node,
 *   --renderHead(columnIndex: number) => node,
 *   --searchable: boolean | (searchText: string, colData: object, rowData: object, rowIndex: number, colIndex: number) => boolean,
 *   --order: keyof Orders,
 *   --sortable: boolean | (lhs: any, rhs: any) => number
 *   }[]
 *   className: string
 *   classes: {
 *   --wrapper: string,
 *   --table: string,
 *   --head: string,
 *   --row(rowData: any, rowIndex: number) => string,
 *   --search: string,
 *   --pagination: string,
 *   --sizePicker: string
 *   }
 *   error(error: { message: string, status (HTTP or Adler32): number }) => node
 *   filters: ((row: object, rowIndex: number, data: object[]) => boolean)[]
 *   pagination: boolean | number
 *   searchable: boolean
 *   sortable: boolean
 *   defaultPage: number
 *   responsive: boolean
 *   style: object
 *   child - the index of data that has an expanded child row: number | false
 * <h2>Events</h2>
 *   onRowClick(rowData: object, rowIndex: number) => any
 */
class DataTable extends Component {

  // Mount
  state = {
    rowsPerPage: 10,
    page: 1,
    searchText: '',
    columns: [{}],
    data: [{}],
    filters: [() => true],
    orders: [],
    classes: {},
    childIndex: false
  }
  constructor(props) {
    super(props)
    const columns = DataTable.getColumns(props, props.columns ? props.columns.map(col => col.order) : [])
    const filters = DataTable.getFilters(props, this.state, columns)
    const data = DataTable.getData(props, this.state, filters, columns).data
    this.state.orders = columns.map(col => col.order || Orders.none)
    this.state.rowsPerPage = DataTable.getRowsPerPage(props, 0, data)
    const numPages = Math.ceil(data.length / this.state.rowsPerPage)
    if (numPages > 0) {
      this.state.page = this.props.defaultPage > numPages ? numPages
        : this.props.defaultPage < 1 ? 1 : this.props.defaultPage
    }
  }

  // Derive
  static checkInput(props) {
    const { columns, pagination } = props
    if (columns !== undefined)
      columns.forEach(col => {
        if (col.key === undefined && col.render === undefined)
          throw new Error('Column object must have either a key or render function')
      })
    if (typeof pagination === 'number' && pagination < 1)
      throw new Error('Pagination must be positive')
  }
  static getColumns(props, orders) {
    const { columns, data, sortable } = props

    return (
      columns === undefined ? keys(data).map(key => ({ key })) : columns.slice()
    ).map((col, c) => ({
      ...col,
      order: orders[c],
      sortable: col.sortable === undefined ? sortable : col.sortable
    }))
  }
  static getFilters(props, state, columns) {
    const { searchable, filters: propFilters } = props
    const { searchText } = state

    const filters = propFilters.slice()
    if (searchable !== false) {
      const matches = searchText.split(' ').filter(txt => txt !== '')
      filters.push(
        ...matches.map(text => (row, r) => {
          let str = ''
          for (let c = 0; c < columns.length; ++c) {
            const col = columns[c]
            if (col.searchable === false) continue
            if (typeof col.searchable === 'function') {
              if (col.searchable(text, resolveKey(row, col.key), row, r, c)) return true
            }
            else if (col.render !== undefined)
              str += innerText(col.render(resolveKey(row, col.key), r, c, row))
            else {
              let value = resolveKey(row, col.key)
              if (value !== undefined && value !== null)
                str += resolveKey(row, col.key).toString()
            }
          }
          return str.toLowerCase().includes(text.toLowerCase())
        })
      )
    }

    return filters
  }
  static getData(props, state, filters, columns) {
    const { orders } = state

    // Filter data
    let data = props.data.slice()
    const selectedRow = props.child !== false && data[props.child]
    for (let i = 0; i < filters.length; ++i) {
      const filter = filters[i]
      data = data.filter(filter)
    }

    // Sort data
    for (let i = 0; i < columns.length; ++i) {
      const col = columns[i]
      if (col.sortable === false || orders[i] === Orders.none) continue
      if (typeof col.sortable === 'function')
        data.sort((lhs, rhs) => col.sortable(resolveKey(lhs, col.key), resolveKey(rhs, col.key)))
      else data.sort((lhs, rhs) => defaultComparator(resolveKey(lhs, col.key), resolveKey(rhs, col.key)))
      if (orders[i] === Orders.desc) data.reverse()
    }

    const childIndex = selectedRow !== false && data.indexOf(selectedRow)
    return {
      data,
      childIndex
    }
  }
  static getClasses(props) {
    const { classes, className } = props
    const container = classList('wolf-data-table', 'h-100', className)
    const wrapper = classList('wolf-data-table-wrapper', 'table-responsive mh-100', classes.wrapper)
    const table = classList(classes.table)
    const sizePicker = classList('mb-3', 'float-left', classes.sizePicker)
    const search = classList('mb-3', 'float-right', 'col-6', 'col-lg-4', classes.search)
    const pagination = classList('mt-3', classes.pagination)
    return {
      ...classes,
      container,
      wrapper,
      table,
      sizePicker,
      search,
      pagination
    }
  }
  static getRowsPerPage(props, rowsPerPage, data) {
    const calculated = props.pagination === false ? data.length
      : props.pagination === true ? defaultProps.pagination
        : props.pagination > data.length ? data.length : props.pagination
    return rowsPerPage || calculated
  }
  static getDerivedStateFromProps(props, state) {
    DataTable.checkInput(props)
    const columns = DataTable.getColumns(props, state.orders)
    const filters = DataTable.getFilters(props, state, columns)
    const { data, childIndex } = DataTable.getData(props, state, filters, columns)
    const classes = DataTable.getClasses(props, state)
    const rowsPerPage = DataTable.getRowsPerPage(props, state.rowsPerPage, data)
    return {
      ...state,
      columns,
      filters,
      data,
      childIndex,
      classes,
      rowsPerPage
    }
  }

  // Render
  render() {
    let { childIndex } = this.state
    const { columns, page, rowsPerPage, classes, orders, data } = this.state
    const { pagination, searchable, error, defaultPage, onRowClick, style, children } = this.props

    const firstRowIndex = (page - 1) * rowsPerPage
    const headColumns = columns.map((col, c) => ({
      classes: col.classes && col.classes.head,
      sortable: col.sortable,
      order: orders[c],
      render: c => {
        if (col.key === undefined && col.renderHead === undefined) return
        if (col.renderHead === undefined) return toReadable(col.key)
        return col.renderHead(c)
      }
    }))
    const bodyColumns = columns.map(col => ({
      key: col.key,
      render: col.render,
      className: col.classes && col.classes.body
    }))
    const numPages = Math.ceil(data.length / rowsPerPage)
    if (childIndex !== false && childIndex >= firstRowIndex && childIndex < firstRowIndex + rowsPerPage)
      childIndex %= rowsPerPage
    else childIndex = false

    return (
      <div className={classes.container} style={style}>
        { pagination &&
        <SizePicker maxSize={data.length}
                    className={classes.sizePicker}
                    defaultSize={rowsPerPage}
                    onChange={this.handlePageSizeChange}
                    value={rowsPerPage}
                    options={[5, 10, 20, 50, 100, 250, 500]} />
        }
        { searchable && <SearchInput className={classes.search} onChange={this.handleSearch} /> }
        <div className={classes.wrapper}>
          <Table className={classes.table}>
            <DataTableHead columns={headColumns}
                           className={classes.head}
                           onHeadCellClick={this.handleToggleSort} />
            <DataTableBody data={data.slice(firstRowIndex, firstRowIndex + rowsPerPage)}
                           columns={bodyColumns}
                           rowClassName={classes.row}
                           error={error}
                           onRowClick={onRowClick}
                           child={childIndex}>{ children }</DataTableBody>
          </Table>
        </div>
        { pagination &&
        <PaginationButtons numPages={numPages}
                           defaultPage={defaultPage}
                           className={classes.pagination}
                           onChange={this.handlePageChange}
                           style={{ paddingLeft: 15 }} />
        }
      </div>
    )
  }

  // Event
  handleToggleSort = c => {
    const orders = this.state.orders.slice()
    orders[c] = orders[c] === Orders.asc ? Orders.desc : Orders.asc
    for (let i = 0; i < orders.length; ++i)
      i !== c && (orders[i] = Orders.none)
    this.setState({ ...this.state, orders })
  }
  handleSearch = searchText => {
    this.setState({ ...this.state, searchText })
  }
  handlePageChange = page => {
    this.setState({ ...this.state, page })
  }
  handlePageSizeChange = rowsPerPage => {
    let { page } = this.state
    const { data } = this.state
    const numPages = Math.ceil(data.length / rowsPerPage)
    page = page > numPages ? numPages : page
    this.setState({ ...this.state, rowsPerPage, page })
  }

}

DataTable.propTypes = propTypes
DataTable.defaultProps = defaultProps

export default DataTable
