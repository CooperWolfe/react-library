import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from 'reactstrap'
import Orders from '../../enums/orders'
import { classList } from '../../utils/stringUtil'

const propTypes = {
  // Input
  columns: PropTypes.arrayOf(PropTypes.shape({
    render: PropTypes.func,
    classes: PropTypes.func,
    sortable: PropTypes.bool,
    order: PropTypes.oneOf([Orders.none, Orders.asc, Orders.desc])
  })).isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
  // Events
  onHeadCellClick: PropTypes.func
}
const defaultProps = {
  className: '',
  style: {},
  onHeadCellClick() {}
}

/**
 * <h2>Input</h2>
 *   columns (required): {
 *   --render(columnIndex: number) => node,
 *   --classes(columnIndex: number) => string,
 *   --sortable: bool,
 *   --order: keyof Orders
 *   }[]
 *   classes: string
 *   style: object
 * <h2>Events</h2>
 *   onHeadCellClick(columnIndex: number) => any
 */
const DataTableHead = ({ className, columns, onHeadCellClick, style }) => {
  const getIconName = order => {
    switch (order) {
    case Orders.asc: return 'fa fa-sort-asc'
    case Orders.desc: return 'fa fa-sort-desc'
    default: return 'fa fa-sort'
    }
  }
  const renderHeadCell = (col, c) => {
    const iconStyle = {
      position: 'absolute',
      top: 0, bottom: 0, right: 8,
      height: 'fit-content',
      margin: 'auto'
    }
    const content = col.render !== undefined && col.render(c)

    return (
      <th key={c}
          className={`position-relative ${classList(col.classes && col.classes(c))}`}
          onClick={() => col.sortable && onHeadCellClick(c)}>
        { col.sortable
          ? (<Row><Col xs={10}>{ content }</Col><i className={getIconName(col.order)} style={iconStyle}/></Row>)
          : content }
      </th>
    )
  }

  return (
    <thead className={classList(className)} style={style}>
    <tr>{ columns.map((col, c) => renderHeadCell(col, c)) }</tr>
    </thead>
  )
}

DataTableHead.propTypes = propTypes
DataTableHead.defaultProps = defaultProps

export default DataTableHead