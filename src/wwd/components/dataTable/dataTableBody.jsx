import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Alert from '../alert'
import NoDataError from '../../errors/noDataError'
import { keys } from '../../utils/arrayUtil'
import { classList } from '../../utils/stringUtil'
import { resolveKey } from '../../utils/objectUtil'

const propTypes = {
  // Input
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  columns: PropTypes.arrayOf(PropTypes.object),
  error: PropTypes.func,
  rowClassName: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object,
  child: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.number
  ]),
  // Events
  onRowClick: PropTypes.func
}
const defaultProps = {
  className: '',
  rowClassName() {},
  style: {},
  child: false,
  onRowClick() {}
}

/**
 * <h2>Input</h2>
 *   data (required): object[]
 *   columns: {
 *   --key (data key): string,
 *   --render(columnData: object, rowIndex: number, columnIndex: number, rowData: object) => node,
 *   --className(columnData: object, rowIndex: number, columnIndex: number, rowData: object) => string
 *   }[]
 *   rowClassName(rowData: object, rowIndex: number) => string
 *   error(error: { message: string, status (HTTP or Adler32): number }) => node
 *   className: string
 *   style: object
 * <h2>Events</h2>
 *   onRowClick(rowData: object, rowIndex: number) => any
 */
class DataTableBody extends Component {

  // Mount
  state = {
    columns: [{}],
    rowClassNames: ['']
  }

  // Derive
  static checkInput(props) {
    const { columns } = props
    if (columns !== undefined)
      columns.forEach(col => {
        if (col.key === undefined && col.render === undefined)
          throw new Error('Column object must have either a key or render function')
      })
  }
  static getColumns(props) {
    const { data, columns } = props
    return columns === undefined
      ? keys(data).map(key => ({ key }))
      : columns.slice()
  }
  static getRowClassNames(props) {
    const { data, rowClassName } = props
    return data.map((row, r) => {
      let classes = classList(rowClassName(row, r))
      if (classes.split(' ').filter(clazz => clazz.substr(0, 6) === 'table-').length > 0
        && row.className !== undefined
        && row.className.split(' ').filter(clazz => clazz.substr(0, 6) === 'table-').length > 0)
        classes = classes.split(' ').filter(clazz => clazz.substr(0, 6) !== 'table-').join(' ')
      return classList(classes, row.className)
    })
  }
  static getDerivedStateFromProps(props, state) {
    DataTableBody.checkInput(props)
    const columns = DataTableBody.getColumns(props)
    const rowClassNames = DataTableBody.getRowClassNames(props)
    return {
      ...state,
      columns,
      rowClassNames
    }
  }

  // Render
  renderCell(row, col, r, c) {
    const className = col.className === undefined ? '' : col.className(resolveKey(row, col.key), r, c, row)
    if (col.render === undefined) return <td key={c} className={className}>{resolveKey(row, col.key)}</td>
    const node = col.render(resolveKey(row, col.key), r, c, row)
    return <td key={c} className={className}>{node}</td>
  }
  renderRow(row, r) {
    const { onRowClick, child, children } = this.props
    const { columns, rowClassNames } = this.state

    return (
      <React.Fragment key={r}>
        <tr className={rowClassNames[r]} onClick={() => onRowClick(row, r)}>
          { columns.map((col, c) => this.renderCell(row, col, r, c)) }
        </tr>
        {r === child && <tr><td colSpan={columns.length}>{ children }</td></tr>}
      </React.Fragment>
    )
  }
  render() {
    const { columns } = this.state
    const { error, data, className, style } = this.props

    return data.length
      ? (<tbody className={className} style={style}>{data.map((row, r) => this.renderRow(row, r))}</tbody>)
      : (<tbody className={className} style={style}>
      <tr>
        <td colSpan={columns.length} className='text-center'>
          <Alert error={new NoDataError()} render={error}/>
        </td>
      </tr>
      </tbody>)
  }
}

DataTableBody.propTypes = propTypes
DataTableBody.defaultProps = defaultProps

export default DataTableBody
