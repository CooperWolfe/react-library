import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const propTypes = {
  linkStyle: PropTypes.object,
  className: PropTypes.string
}
const defaultProps = {
  linkStyle: {},
  className: ''
}

class NotFound extends Component {
  render() {
    const { linkStyle, className, ...props } = this.props

    return (
      <div className={`text-center ${className}`} { ...props }>
        <div className='w-100 h1'>Whoops!</div>
        <p>We couldn't find your page. Why don't you go back <Link to='/' style={linkStyle}>home</Link>?</p>
      </div>
    )
  }
}

NotFound.propTypes = propTypes
NotFound.defaultProps = defaultProps

export default NotFound
