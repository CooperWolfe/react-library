import React, { Component } from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
  style: PropTypes.object,
  imgStyle: PropTypes.object,
  textStyle: PropTypes.object,
  className: PropTypes.string
}
const defaultProps = {
  alt: '',
  style: {},
  imgStyle: {},
  textStyle: {},
  className: ''
}

class Header extends Component {
  render() {
    const { src, alt, children, className } = this.props
    let { imgStyle, textStyle, style } = this.props

    imgStyle = {
      objectFit: 'cover',
      objectPosition: 'center',
      position: 'absolute',
      top: 0, bottom: 0, left: 0, right: 0,
      width: '100%',
      height: 500,
      ...imgStyle
    }
    textStyle = {
      left: 0, right: 0, top: 0, bottom: 0,
      padding: '200px 10%',
      position: 'absolute',
      fontFamily: '"Courgette", cursive',
      fontSize: '4rem',
      ...textStyle
    }
    style = {
      height: imgStyle.height,
      ...style
    }

    return (
      <div className={`header position-relative ${className}`} style={style}>
        <img src={src} alt={alt} style={imgStyle} />
        <div style={textStyle}>{ children }</div>
      </div>
    )
  }
}

Header.propTypes = propTypes
Header.defaultProps = defaultProps

export default Header
