import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  InputGroup,
  Input,
  InputGroupButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap'
import { classList } from '../../utils/stringUtil'

const propTypes = {
  // Input
  maxSize: PropTypes.number,
  className: PropTypes.string,
  style: PropTypes.object,
  defaultSize: PropTypes.number,
  options: PropTypes.arrayOf(PropTypes.number),
  // Events
  onChange: PropTypes.func
}
const defaultProps = {
  maxSize: Number.MAX_VALUE,
  className: '',
  defaultSize: 10,
  style: {},
  options: [5, 10, 20, 50, 100, 200, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000],
  onChange() {}
}

/**
 * <h2>Input</h2>
 * value: number
 * maxSize: number
 * className: string
 * defaultSize: number
 * style: object
 * options: number[]
 * <h2>Events</h2>
 * onChange(size: number) => any
 */
class SizePicker extends Component {
  // Mount
  state = {
    dropdownOpen: false,
    options: [1, 2, 3]
  }

  // Derive
  static getOptions(props) {
    return props.options.filter(opt => opt <= props.maxSize)
  }
  static getDerivedStateFromProps(props, state) {
    const options = SizePicker.getOptions(props)
    return { ...state, options }
  }

  // Render
  render() {
    const { dropdownOpen, options } = this.state
    const { className, style, value } = this.props

    return (
      <InputGroup className={classList('col-5', 'col-sm-4', 'col-md-3', 'col-lg-2', className)} style={style}>
        <Input type='number' value={value} onChange={e => this.handleInputChange(+e.target.value)}/>
        { options.length > 1 &&
        <InputGroupButtonDropdown addonType='append'
                                  isOpen={dropdownOpen}
                                  toggle={e => [this.handleToggleDropdown(), this.handleDropdownSelect(e)]}>
          <DropdownToggle caret/>
          <DropdownMenu>
            { options.map(opt => (
              <DropdownItem data-wwd-size={opt} key={opt}>{ opt }</DropdownItem>
            )) }
          </DropdownMenu>
        </InputGroupButtonDropdown>}
      </InputGroup>
    )
  }

  // Events
  handleToggleDropdown = () => {
    const { dropdownOpen } = this.state
    this.setState({ ...this.state, dropdownOpen: !dropdownOpen })
  }
  handleInputChange = size => {
    const { onChange, maxSize } = this.props
    if (size <= 0 || size > maxSize) return
    onChange(size)
  }
  handleDropdownSelect = event => {
    const { onChange } = this.props
    if (!event || !event.target || !event.target.getAttribute) return
    const size = +event.target.getAttribute('data-wwd-size')
    if (size <= 0) return
    onChange(size)
  }
}

SizePicker.propTypes = propTypes
SizePicker.defaultProps = defaultProps

export default SizePicker