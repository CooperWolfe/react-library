import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  InputGroup,
  InputGroupText,
  InputGroupAddon,
  Input
} from 'reactstrap'
import { classList } from '../../utils/stringUtil'

const propTypes = {
  // Input
  className: PropTypes.string,
  style: PropTypes.object,
  defaultValue: PropTypes.string,
  placeHolder: PropTypes.string,
  // Events
  onChange: PropTypes.func
}
const defaultProps = {
  // Input
  className: '',
  style: {},
  defaultValue: '',
  placeHolder: 'Search...',
  // Events
  onChange() {}
}

/**
 * <h2>Input</h2>
 * className: string
 * defaultValue: string
 * placeHolder: string
 * style: object
 * <h2>Events</h2>
 * onChange(text: string) => any
 */
class SearchInput extends Component {

  state = {
    value: this.props.defaultValue
  }

  handleValueChange = value => {
    const { onChange } = this.props
    this.setState({ ...this.state, value })
    onChange(value)
  }

  render() {
    const { className, placeHolder, style } = this.props
    const { value } = this.state
    return (
      <InputGroup className={classList(className)} style={style}>
        <InputGroupAddon addonType='prepend'>
          <InputGroupText>
            <i className='fa fa-search'/>
          </InputGroupText>
        </InputGroupAddon>
        <Input placeholder={placeHolder} onChange={e => this.handleValueChange(e.target.value)} value={value}/>
      </InputGroup>
    )
  }
}

SearchInput.propTypes = propTypes
SearchInput.defaultProps = defaultProps

export default SearchInput