import React, { Component } from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  imageUrl: PropTypes.string.isRequired
}
const defaultProps = {}

class Parallax extends Component {
  render() {
    const { imageUrl, style, ...props } = this.props

    return (
      <div {...props} style={{
        backgroundAttachment: 'fixed',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundImage: `url(${imageUrl})`,
        ...style
      }}/>
    )
  }
}

Parallax.propTypes = propTypes
Parallax.defaultProps = defaultProps

export default Parallax
