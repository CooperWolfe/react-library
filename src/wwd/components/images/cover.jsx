import React, { Component } from 'react'
import PropTypes from 'prop-types'

const propTypes = {
  imageUrl: PropTypes.string.isRequired
}
const defaultProps = {}

class Cover extends Component {
  render() {
    const { imageUrl, style, ...props } = this.props
    return (
      <div { ...props } style={{
        backgroundImage: `url(${imageUrl})`,
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        ...style
      }}/>
    )
  }
}

Cover.propTypes = propTypes
Cover.defaultProps = defaultProps

export default Cover
