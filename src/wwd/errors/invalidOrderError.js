class InvalidOrderError extends Error {
  static status = 0x3c2406ce
  message = "Order must be 'none', 'asc', or 'desc'. Please use Orders enum."
  status = InvalidOrderError.status
  constructor(message, status) {
    super()
    if (message !== undefined) this.message = message
    if (status !== undefined) this.status = status
  }
}

export default InvalidOrderError