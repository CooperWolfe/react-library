# WWD React Library
The purpose of this library is to provide a series of useful and reusable React Components.

## Alert
A simple h5 with default error text, totally customizable.

## DataTable
A robust data-driven table with sorting, searching, filtering, pagination, and child rows already implemented

## Form
A form helper with automatic validation a render functions for commonly used input fields

## PaginationButtons
A set of customizable pagination-styled buttons

## SearchInput
A Search-styled input field

## SizePicker
A dropdown input field with some handy default values

## ToggleableIcon
An favicon that toggles between filled and outline when clicked
